# PR Checklist

(Insert summary of change here)

- [ ] All of the below checks must be walked through with a reviewer

## Feature -> Dev Checks
- [ ] Jenkins feature branch build is passing
- [ ] Linked to a Jira ticket
- [ ] Test plan attached to Jira
- [ ] Unit tested - business logic
- [ ] Integration tested - individual service
- [ ] System performance impacts considered
- [ ] Security impacts considered
- [ ] Readme updated (if appropriate)
- [ ] Handler/Service/DAO architecture adhered to
- [ ] Lambdas given appropriate permissions
- [ ] Lambdas are not erroring or timing out under certain conditions
- [ ] Changes to resources do not require a stack re-deploy
- [ ] No manual changes made via the AWS Console

## Staging -> Prod Checks
- [ ] Jenkins release branch build is passing
- [ ] Manually tested - UI / Integrations


## Clean up
- [ ] Run `./removeStack.sh <branch>` to remove the CloudFormation stack - this can take up to 20 mins due to VPC NI decommissioning
- [ ] Remove the Serverless S3 bucket
