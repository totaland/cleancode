#!groovy​

def awsProfile() {
  def branchName = "${env.BRANCH_NAME}"
  if (branchName ==~ /(master|hotfix.*)/) {
    return 'prod'
  } else if (branchName ==~ /release.*/) {
    return 'staging'
  } else {
    return 'dev'
  }
}

def integrationHost() {
  def branchName = "${env.BRANCH_NAME}"
  if (branchName ==~ /master/) {
    return 'todo'
  } else if (branchName ==~ /(release.*|hotfix.*)/) {
    return 'todo'
  } else {
    return 'integrationapi-dev.cvignp2m3sio.ap-southeast-2.rds.amazonaws.com'
  }
}

def awsCredential(String profile) {
  return 'AWS_' + profile.toUpperCase();
}

pipeline {
  agent any
  options {
    disableConcurrentBuilds()
  }
  environment {
    AWS_PROFILE = awsProfile()
    AWS_CREDENTIAL_ID = awsCredential(AWS_PROFILE)
    INTEGRATION_API_HOST = integrationHost()
  }
  stages {
    stage('Branch Check') {
      when {
        not { expression { BRANCH_NAME ==~ /(master|hotfix.*|release.*|develop|feature.*)/ } }
      }
      steps {
        error('Invalid branch name!')
      }
    }
    stage('Configure Environment') {
      steps {
        withCredentials([
                [$class           : 'AmazonWebServicesCredentialsBinding',
                 accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                 credentialsId    : AWS_CREDENTIAL_ID,
                 secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
          nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
            sh '''
aws configure set region ap-southeast-2 --profile ${AWS_PROFILE}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID} --profile ${AWS_PROFILE}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY} --profile ${AWS_PROFILE}
'''
          }
        }
      }
    }
    stage('Compile') {
      steps {
        nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
          sh 'yarn install && yarn run compile'
        }
      }
    }
/*
        stage('Unit Test') {
            steps {
                nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
                    sh 'yarn run unit'
                }
            }
        }
*/
    stage('Create Resources') {
      when {
        expression { BRANCH_NAME ==~ /(develop|release.*|master)/ }
      }
      steps {
        nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
          sh 'yarn run sls deploy additionalstacks -s ${AWS_PROFILE} --profile ${AWS_PROFILE} -v'
        }
      }
    }
    stage('Liquibase') {
      steps {
        liquibaseUpdate changeLogFile: 'liquibase/db.master.xml', databaseEngine: 'PostgreSQL', url: "jdbc:postgresql://${INTEGRATION_API_HOST}/", credentialsId: "integration-api-${AWS_PROFILE}"
      }
    }
    stage('Integration Tests') {
      steps {
        nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
          sh 'yarn run integration -s ${AWS_PROFILE} --profile ${AWS_PROFILE}'
        }
      }
    }
    stage('Deploy Domain') {
      when {
        expression { BRANCH_NAME ==~ /(develop|release.*|master)/ }
      }
      steps {
        nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
          sh 'yarn run sls create_domain -s ${AWS_PROFILE} --profile ${AWS_PROFILE} -v'
        }
      }
    }
    stage('Deploy') {
      when {
        expression { BRANCH_NAME ==~ /(develop|release.*|master)/ }
      }
      steps {
        nodejs(nodeJSInstallationName: 'NodeJS 10.15.3') {
          sh 'yarn run sls deploy -s ${AWS_PROFILE} --profile ${AWS_PROFILE} -v --skip-additionalstacks'
        }
      }
    }
  }
}
