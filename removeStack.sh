#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Usage: $(basename $0) <feature branch name>"
    exit 1
fi

echo "::Deleting Serverless Stack::"
SLS="
service:
  name: integration-api-feature-$1
provider:
  name: aws
  region: 'ap-southeast-2'
"
echo "$SLS" > removestack-serverless.yml
./node_modules/.bin/sls --config removestack-serverless.yml remove -v
rm removestack-serverless.yml
cd ..
