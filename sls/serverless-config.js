// Serverless service names with slash characters '/' cause an error during packaging
// This is relevant as we use Git flow style branch naming and use the branch name as the service name for multipipeline deployment
module.exports.gitBranchDash = async (serverless) => {
  var branch = await serverless.variables.getValueFromSource('git:branch');
  if (branch.includes('release') || branch.includes('master') || branch.includes('hotfix')) {
    return '';
  }
  branch = branch.replace('/', '-');
  branch = branch.replace(/\./g, '-');
  branch = branch.toLowerCase();
  return '-'.concat(branch);
};

module.exports.enableCustomDomain = async (serverless) => {
  var branch = await serverless.variables.getValueFromSource('git:branch');
  return branch.includes('develop') || branch.includes('release') || branch.includes('master');
};
