import {Substitute} from '@fluffy-spoon/substitute';
import {logger} from 'app/util/logger';
import {Client, QueryConfig, QueryResult} from 'pg';

export abstract class AccessContext {
  protected constructor(protected readonly client = process.env.MOCK_RDS_CLIENT ? Substitute.for<Client>() : new Client()) {
  }

  public abstract async initialise(): Promise<void>;

  public async query(config: QueryConfig): Promise<QueryResult> {
    logger.debug('query', config);
    return await this.client.query(config);
  }

  public abstract async finalise(error?: Error): Promise<void>;
}
