import {AccessContext} from 'app/access/AccessContext';
import {logger} from 'app/util/logger';

export class AtomicAccessContext extends AccessContext {
    public constructor() {
        super();
    }

    public async initialise(): Promise<void> {
      await this.client.connect();
    }

    public async finalise(): Promise<void> {
        await this.client.end();
    }

}
