import {AccessContext} from 'app/access/AccessContext';
import {logger} from 'app/util/logger';

export class TransactionalAccessContext extends AccessContext {
    public constructor() {
        super();
    }

    public async initialise(): Promise<void> {
        await this.client.connect();
        logger.info('beginning new transaction');
        await this.client.query('BEGIN');
    }

    public async finalise(error?: Error): Promise<void> {
        try {
            if (error) {
                logger.error('rolling back transaction');
                await this.client.query('ROLLBACK');
            } else {
                logger.info('committing transaction');
                await this.client.query('COMMIT');
            }
        } catch (err) {
            logger.error('Encountered a failure while attempting to finalise transaction');
            throw err;
        } finally {
            await this.client.end();
        }
    }

}
