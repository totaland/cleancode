import {Position} from 'app/entities/Position';
import {IntegrationError} from 'app/errors/IntegrationError';
import {logger} from 'app/util/logger';
import fetch from 'node-fetch';

export interface IPositionApiDao {
  create(position: Position): Promise<Position>;
}

const publicApi = process.env.ADS_URL;

export class PositionApiDaoImpl implements IPositionApiDao {

  public async create(position: Position): Promise<Position> {
    const postBody = {
      positionName: position.positionName,
    };
    logger.info(`sending to [${publicApi}]`, postBody);
    const fetchParams: any = {
      method: 'POST',
      body: JSON.stringify(postBody),
    };
    const fetchResult = await fetch(`${publicApi}/candidates`, fetchParams); // example only - todo replace with actual endpoint
    if (fetchResult.status !== 201) {
      logger.error(`Failed to send [${position.positionName}]: ${fetchResult.statusText}` + await fetchResult.text(), position);
      throw new IntegrationError(`Failed to send [${position.positionName}]: ${fetchResult.statusText}`);
    }
    return fetchResult.json();
  }

}

export const positionApiDao: IPositionApiDao = new PositionApiDaoImpl();
