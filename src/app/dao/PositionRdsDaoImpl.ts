import {AccessContext} from 'app/access/AccessContext';
import {Position} from 'app/entities/Position';
import {logger} from 'app/util/logger';

export interface IPositionRdsDao {
  create(position: Position, context: AccessContext): Promise<void>;
  update(position: Position, context: AccessContext): Promise<void>;
  get(positionId: string, keyField: string, context: AccessContext): Promise<Position[]>;
}

const fields = '"remotePositionId", "locale", "positionName", "jobCategory", "managementLevel", "invitationExpiryDays", "reminderDays", "returnUrl", "completionUrl"';
const insert = `INSERT INTO positions(${fields}) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)`;
const upsert = `${insert} ON CONFLICT ON CONSTRAINT positions_pkey DO UPDATE SET "locale" = EXCLUDED."locale", "positionName" = EXCLUDED."positionName", "jobCategory" = EXCLUDED."jobCategory", "managementLevel" = EXCLUDED."managementLevel", "invitationExpiryDays" = EXCLUDED."invitationExpiryDays", "reminderDays" = EXCLUDED."reminderDays", "returnUrl" = EXCLUDED."returnUrl", "completionUrl" = EXCLUDED."completionUrl"`;

export class PositionRdsDaoImpl implements IPositionRdsDao {

  public async create(position: Position, context: AccessContext): Promise<void> {
    return this.insert(position, false, context);
  }

  public async update(position: Position, context: AccessContext): Promise<void> {
    return this.insert(position, true, context);
  }

  public async get(positionId: string, keyField: string, context: AccessContext): Promise<Position[]> {
    const query = {
      text: `SELECT * FROM positions WHERE "${keyField}" = $1`,
      values: [positionId],
    };
    const result = await context.query(query);
    return result.rows as Position[];
  }

  private async insert(position: Position, allowUpdate: boolean, context: AccessContext): Promise<void> {
    logger.info('creating position', position);
    const query = {
      text: allowUpdate ? upsert : insert,
      values: [position.remotePositionId, position.locale, position.positionName, position.jobCategory, position.managementLevel,
        position.invitationExpiryDays, position.reminderDays, position.returnUrl, position.completionUrl],
    };
    await context.query(query);
  }
}

export const positionRdsDao: IPositionRdsDao = new PositionRdsDaoImpl();
