export default interface ILambdaProxyEssentialResponse {
  body: string;
  statusCode: number;
}
