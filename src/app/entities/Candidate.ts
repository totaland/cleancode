export class Candidate {
    constructor(
        public remoteCandidateId: string,
        public remotePositionId: string,
        public revelianCandidateId: string,
        public revelianPositionId: string,
        public firstName: string,
        public lastName: string,
        public email: string,
        public roundNumber: string,
        public testingStatus: string,
        public resultSent: string,
    ) {}
}
