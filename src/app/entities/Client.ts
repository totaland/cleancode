export class Client {
    constructor(
        public clientId: string,
        public remoteClientId: string,
        public revelianClientId: string,
        public revelianAPIKey: string,
        public remoteAccessToken: string,
        public remoteRefreshToken: string,
        public remoteRefreshExpiry: string,
        public remoteBaseUrl: string,
    ) {}
}
