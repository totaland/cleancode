export class EmailTemplate {
    constructor(
        public id: string,
        public name: string,
        public type: string,
        public subject: string,
        public body: string,
        public clientId: string,
    ) {
    }
}
