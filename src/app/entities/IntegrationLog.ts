export class IntegrationLog {
    constructor(
        public timestamp: string,
        public clientId: string,
        public endpoint: string,
        public requestParams: string,
        public requestPayload: string,
        public responseParams: string,
        public responsePayload: string,
    ) {}
}
