export class Locale {
    constructor(
        public region: string,
        public language: string,
    ) {}
}
