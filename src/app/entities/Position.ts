import {Locale} from 'app/entities/Locale';

export class Position {
  constructor(
    public remotePositionId: string,
    public locale: string,
    public positionName: string,
    public jobCategory: string,
    public managementLevel: string,
    public invitationExpiryDays: number,
    public reminderDays: number,
    public returnUrl?: string,
    public completionUrl?: string,
  ) {
  }
}
