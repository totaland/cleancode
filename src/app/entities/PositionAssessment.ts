export class PositionAssessment {
    constructor(
        public remotePositionId: string,
        public revelianAssessmentId: string,
        public packages: string,
        public benchmark: string,
    ) {}
}
