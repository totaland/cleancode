export class PositionDemographics {
    constructor(
        public remotePositionId: string,
        public demographicId: string,
        public value: string,
    ) {}
}
