import { IntegrationError } from './IntegrationError';

export class NotFoundError extends IntegrationError {
  public statusCode: number;

  constructor(message: string) {
    super(message);
    this.statusCode = 404;
  }
}
