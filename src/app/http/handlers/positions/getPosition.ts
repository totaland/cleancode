import middy from '@middy/core';
import cors from '@middy/http-cors';
import httpErrorHandler from '@middy/http-error-handler';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpHeaderNormalizer from '@middy/http-header-normalizer';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import warmup from '@middy/warmup';
import {positionService} from 'app/services/PositionService';
import {validator} from 'app/util/proxySafeValidator';
import {errorResponseAsync, ok} from 'app/util/response';
import responseNormalizer from 'app/util/responseNormalizer';
import {Callback, Context, Handler} from 'aws-lambda';

interface IValidatedGatewayEvent {
  pathParameters: {
    remotePositionId: string,
  };
  headers: {
    authorization: string,
  };
}

const getPosition: Handler = async (event: IValidatedGatewayEvent, _: Context, cb: Callback) => {
  try {
    return ok(cb, await positionService.getPosition(event.pathParameters.remotePositionId));
  } catch (error) {
    return errorResponseAsync(error);
  }
};

const inputSchema = {
  type: 'object',
  required: ['pathParameters'],
  properties: {
    pathParameters: {
      type: 'object',
      required: ['remotePositionId'],
      properties: {
        remotePositionId: {
          type: 'string',
        },
      },
    },
    headers: {
      type: 'object',
      required: ['authorization'],
      properties: {
        authorization: {
          type: 'string',
        },
      },
    },
  },
};

export const handler = middy(getPosition)
  .use(warmup())
  .use(httpJsonBodyParser({}))
  .use(httpHeaderNormalizer())
  .use(httpEventNormalizer())
  .use(validator({inputSchema}))
  .use(httpErrorHandler())
  .use(responseNormalizer())
  .use(cors());

export default getPosition;
