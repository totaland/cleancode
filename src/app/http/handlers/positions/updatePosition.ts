import middy from '@middy/core';
import cors from '@middy/http-cors';
import httpErrorHandler from '@middy/http-error-handler';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpHeaderNormalizer from '@middy/http-header-normalizer';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import warmup from '@middy/warmup';
import {Position} from 'app/entities/Position';
import {positionService} from 'app/services/PositionService';
import {validator} from 'app/util/proxySafeValidator';
import {errorResponseAsync, noContentAsync} from 'app/util/response';
import responseNormalizer from 'app/util/responseNormalizer';
import {Context, Handler} from 'aws-lambda';

interface IValidatedGatewayEvent {
  pathParameters: {
    remotePositionId: string,
  };
  body: {
    locale: string,
    positionName: string,
    jobCategory: string,
    managementLevel: string,
    invitationExpiryDays: number,
    reminderDays: number,
    returnUrl?: string,
    completionUrl?: string,
  };
  headers: {
    authorization: string,
  };
}

const updatePosition: Handler = async (event: IValidatedGatewayEvent, _: Context) => {
  try {
    await positionService.updatePosition(event.pathParameters.remotePositionId, event.body as Position);
    return noContentAsync();
  } catch (error) {
    return errorResponseAsync(error);
  }
};

const inputSchema = {
  type: 'object',
  required: ['pathParameters', 'body'],
  properties: {
    pathParameters: {
      type: 'object',
      required: ['remotePositionId'],
      properties: {
        remotePositionId: {
          type: 'string',
        },
      },
    },
    headers: {
      type: 'object',
      required: ['authorization'],
      properties: {
        authorization: {
          type: 'string',
        },
      },
    },
    body: {
      type: 'object',
      required: ['locale', 'positionName', 'jobCategory', 'managementLevel', 'invitationExpiryDays', 'reminderDays'],
      properties: {
        locale: {
          type: 'string',
        },
        positionName: {
          type: 'string',
        },
        jobCategory: {
          type: 'string',
        },
        managementLevel: {
          type: 'string',
        },
        invitationExpiryDays: {
          type: 'number',
        },
        reminderDays: {
          type: 'number',
        },
      },
    },
  },
};

export const handler = middy(updatePosition)
  .use(warmup())
  .use(httpJsonBodyParser({}))
  .use(httpHeaderNormalizer())
  .use(httpEventNormalizer())
  .use(validator({inputSchema}))
  .use(httpErrorHandler())
  .use(responseNormalizer())
  .use(cors());

export default updatePosition;
