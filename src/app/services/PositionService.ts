import {AtomicAccessContext} from 'app/access/AtomicAccessContext';
import {IPositionApiDao, positionApiDao} from 'app/dao/PositionApiDaoImpl';
import {IPositionRdsDao, positionRdsDao} from 'app/dao/PositionRdsDaoImpl';
import {Position} from 'app/entities/Position';
import {NotFoundError} from 'app/errors/NotFound';

export interface IPositionService {
  createPositionApi(position: Position): Promise<Position>;
  createPosition(position: Position): Promise<void>;
  updatePosition(positionId: string, position: Position): Promise<void>;
  getPosition(positionId: string): Promise<Position>;
}

export class PositionServiceImpl implements IPositionService {

  constructor(private readonly positionsRdsDao: IPositionRdsDao,
              private readonly positionsApiDao: IPositionApiDao) {
  }

  // currently unused
  public async createPositionApi(position: Position): Promise<Position> {
    return await this.positionsApiDao.create(position);
  }

  public async createPosition(position: Position): Promise<void> {
    const context = new AtomicAccessContext();
    await context.initialise();
    try {
      await this.positionsRdsDao.create(position, context);
    } finally {
      await context.finalise();
    }
  }

  public async updatePosition(positionId: string, position: Position): Promise<void> {
    position.remotePositionId = positionId;
    const context = new AtomicAccessContext();
    await context.initialise();
    try {
      await this.positionsRdsDao.update(position, context);
    } finally {
      await context.finalise();
    }
  }

  public async getPosition(positionId: string): Promise<Position> {
    const context = new AtomicAccessContext();
    let position: Position;
    await context.initialise();
    try {
      const positions = await this.positionsRdsDao.get(positionId, 'remotePositionId', context);
      if (positions[0]) {
        position = positions[0];
      } else {
        throw new NotFoundError(`Could not find position with id ${positionId}`);
      }
    } finally {
      await context.finalise();
    }
    return position;
  }

}

export const positionService: IPositionService = new PositionServiceImpl(positionRdsDao, positionApiDao);
