export interface ILogger {
    debug(logLine: string, ...args: object[]): void;
    error(logLine: string | Error, ...args: object[]): void;
    info(logLine: string, ...args: object[]): void;
    warn(logLine: string, ...args: object[]): void;
}

class ConsoleLogger implements ILogger {

    public debug(logLine: string, ...args: object[]): void {
        /* tslint:disable */ console.debug('[DEBUG] ' + logLine, args); /* tslint:enable */
    }

    public error(logLine: string | Error, ...args: object[]): void {
        /* tslint:disable */ console.error('[ERROR] ' + logLine, args); /* tslint:enable */
    }

    public info(logLine: string, ...args: object[]): void {
        /* tslint:disable */ console.info('[INFO] ' + logLine, args); /* tslint:enable */
    }

    public warn(logLine: string, ...args: object[]): void {
        /* tslint:disable */ console.warn('[WARN] ' + logLine, args); /* tslint:enable */
    }
}

export const logger: ILogger = new ConsoleLogger();
