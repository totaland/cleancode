import { APIGatewayEvent, Callback, Context } from 'aws-lambda';

import Ajv from 'ajv';
import { Options as AjvOptions } from 'ajv';

import ILambdaProxyEssentialResponse from 'app/defs/LambdaProxyEssentialResponse';

interface IValidatorOptions {
  inputSchema?: any;
  ajvOptions?: Partial<AjvOptions>;
}

interface IHandler {
  event: APIGatewayEvent;
  context?: Context;
  callback: Callback;
}

const defaults = {
  v5: true,
  coerceTypes: 'array', // important for query string params
  allErrors: true,
  useDefaults: true,
  $data: true, // required for ajv-keywords
  defaultLanguage: 'en',
};

export const uuid4Pattern = { type: 'string', length: 32, pattern: '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$' };

export const validator = ({ inputSchema, ajvOptions }: IValidatorOptions) => {
  const options = Object.assign({}, defaults, ajvOptions);
  const ajv = new Ajv(options);
  const validateInput = inputSchema ? ajv.compile(inputSchema) : null;

  return {
    before(handler: IHandler, next: any) {
      if (!inputSchema || !validateInput) {
        return next();
      }

      const valid = validateInput(handler.event);

      if (!valid) {
        const response: ILambdaProxyEssentialResponse = {
          body: JSON.stringify({
            message: validateInput.errors,
          }),
          statusCode: 400,
        };
        return handler.callback(null, response);
      }

      return next();
    },
  };
};
