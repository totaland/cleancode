import {IntegrationError} from 'app/errors/IntegrationError';
import {logger} from 'app/util/logger';
import {Callback} from 'aws-lambda';

export class HttpResponse {
  constructor(public statusCode: number, public body: any, public headers?: any) { }
}

export function respond(cb: Callback, statusCode: number, responseBody: any): void {
  cb(null, new HttpResponse(statusCode, responseBody));
}

export function ok(cb: Callback, responseBody: any): void {
  respond(cb, 200, responseBody);
}

export function okAsync(responseBody: any): HttpResponse {
  return new HttpResponse(200, responseBody);
}

export function created(cb: Callback, responseBody: any): void {
  respond(cb, 201, responseBody);
}

export function createdAsync(responseBody: any): HttpResponse {
  return new HttpResponse(201, responseBody);
}

export function noContentAsync(): HttpResponse {
  return new HttpResponse(204, null);
}

export function badRequest(cb: Callback, message: string): void {
  respond(cb, 400, message);
}

export function notFound(cb: Callback, message: string): void {
  respond(cb, 404, message);
}

export function notFoundAsync(message: string): HttpResponse {
  return new HttpResponse(404, message);
}

export function errorResponse(cb: Callback, error: Error): void {
  logger.error(error);
  let statusCode: number = 500;
  if (error instanceof IntegrationError) {
    statusCode = error.statusCode;
  }
  respond(cb, statusCode, error.message);
}

export function errorResponseAsync(error: Error): HttpResponse {
  logger.error(error);
  let statusCode: number = 500;
  if (error instanceof IntegrationError) {
    statusCode = error.statusCode;
  }
  return new HttpResponse(statusCode, error.message);
}

// When using lambda integration (as opposed to default lambda-proxy) a special format is required for errors
// so that they can be mapped to http response codes correctly
export function errorResponseLambdaIntegration(error: Error): Error {
  logger.error(error);
  let statusCode: number = 500;
  if (error instanceof IntegrationError) {
    statusCode = error.statusCode;
  }
  return new Error(`[${statusCode}] ${error.message}`);
}
