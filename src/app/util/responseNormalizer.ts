export default () => ({
  after: (handler: any, next: any) => {
    const { response } = handler;

    if (response.hasOwnProperty('body')) {
      handler.response.body = JSON.stringify(response.body);
    }

    return next();
  },
});
