import request from 'supertest';

const url = 'http://localhost:3000';

test.skip('The seed function returns a 200 response', () => {
    return request(url)
        .get('/seed')
        .send()
        .expect(200)
        .then((res) => {
            expect(res.body).toBe('Success');
        });
});
